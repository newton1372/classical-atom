//********************************************************************
//                    A T O M O    C L A S S I C O
//********************************************************************


//Si dimostra che, se valessero le leggi della Meccanica Classica anche su scala atomica,
//l'elettrone dovrebbe irraggiare durante la rotazione, perdendo quindi energia (e quota).
//A conti fatti, la distanza risulterebbe essere del tipo
//r = (a0^3-v*t)^(1/3), dove a0 è il raggio classico dell'atomo, v una 
//certa costante.
//La morale della favola è che, se si trascurasse la meccanica quantistica, 
// l'atomo "collasserebbe" in un tempo dell'ordine di grandezza di 10e-11 s.
//In netto contrasto con l'esperienza, che mostra invece atomi tutti stabili (e identici!)


//Innanzitutto calcoliamo la distanza dell'elettrone nel tempo.
var distance=function(A, B,t)
{
    
    var a0=  Math.sqrt(Math.pow((A.x-B.x),2) + Math.pow((A.y-B.y),2) + Math.pow((A.z-B.z),2));
    var K = 10000;
    if(Math.pow(a0,3)-K*t>0) return Math.pow(Math.pow(a0,3) - K*t,1/3)
    else return 0;
}


//Adesso creiamo l'animazione.

var createScene = function () {
    //Creazione scena e settaggio illuminazione, inquadratura telecamera...
    var scene = new BABYLON.Scene(engine);

    var light = new BABYLON.PointLight("Omni", new BABYLON.Vector3(0, 100, 100), scene);
    var camera = new BABYLON.ArcRotateCamera("Camera", 0, 0.8, 100, new BABYLON.Vector3.Zero(), scene);
    camera.attachControl(canvas, true);
    //Creo dinamicamente le due sfere e imposto la posizione iniziale
    var sphere1 =new BABYLON.Mesh.CreateSphere("Sphere1",10,10, scene);
    sphere1.position.set(0,0,0); //Convenzionalmente il nucleo è nell'origine
    var sphere2 =new BABYLON.Mesh.CreateSphere("Sphere2",10,2, scene);
    //Secondo il modello di Bohr la distanza dell'elettrone dal
    //nucleo è proporzionale al quadrato del numero quantico n.
    var n=2; //Livello energetico: numero quantico.
    sphere2.position.set(-15*Math.pow(n,2),0,0); 
    //Creo tre oggetti animazione, una per ciascuna coordinata. 
    //L'animazione i-esima fa muovere la coordinata i-esima.
    var animationSphereX = new BABYLON.Animation("animationSphereX", "position.x", 30, BABYLON.Animation.ANIMATIONTYPE_FLOAT,
                                                                    BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE);
    var animationSphereY= new BABYLON.Animation("animationSphereY","position.y",30,BABYLON.Animation.ANIMATIONTYPE_FLOAT,
                                                                    BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE);
    var animationSphereZ= new BABYLON.Animation("animationSphereZ","position.z",30,BABYLON.Animation.ANIMATIONTYPE_FLOAT,
                                                                    BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE);
    //Velocità angolare. 
    var w = 2;
    //Raggio atomico
        
    //Definisco i keys (a quanto ho capito, sono poco più che "liste" dinamiche di frame.)
    //KeysI è la lista di frame dentro animationI.
    var keysX = [];
    var keysY = [];
    var keysZ = [];

    //Creazione dei fotogrammi.
    var int=0;
    for(var t=0;t<100;t=t+0.1, int++)
    {
        //Calcolo la distanza tra l'elettrone e l'atomo al tempo t
       var r = distance(sphere1.position,sphere2.position,t);
       keysX.push({
            frame: int,
            value: r*Math.cos(w*t)});

        keysY.push({
            frame:int,
            value: 0});

        keysZ.push({
            frame:int,
            value:r*Math.sin(w*t)});
    }


    //Infilo le liste di frame (keys) dentro i rispettivi oggetti animazione
    animationSphereX.setKeys(keysX);
    animationSphereY.setKeys(keysY);
    animationSphereZ.setKeys(keysZ);

    //Aggiungo le animazioni alla sfera 2
    sphere2.animations.push(animationSphereX);
    sphere2.animations.push(animationSphereY);
    sphere2.animations.push(animationSphereZ);

    //Faccio partire l'animazione
    scene.beginAnimation(sphere2,0,10000, false);

    
    return scene;
}
